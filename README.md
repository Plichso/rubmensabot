# RUBMensaBot

## Requirements
Requirements are saved in requirements.txt

### Installing
```
# python3 -m venv mensaBot
# source mensaBot/bin/activate
pip install -r requirements.txt
```

## Run on startup
The bot is running automaticaly on every startup on my server. 
To archive this behavior, the shell script mensaBot.sh is used.
```
sudo crontab -e
```
Add sth. like:
```
@reboot bash pathToBot/mensaBot.sh
```
