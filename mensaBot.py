import pickle
from telegram.ext import MessageHandler, Filters
import time
import schedule
import feedparser
from datetime import date
from telegram.ext import Updater
import qwest
import holidays
import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

with open('telegram_api.key', 'r') as f:
    api_key = f.read()
updater = Updater(token=api_key.strip())
dispatcher = updater.dispatcher


def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="""Willkommen beim RUB
                     Mensa Bot, hier erfährst du jeden Tag was es in der Mensa
                     gibt. \n Mögliche Befehle: \n \t /register args -
                     Registrieren für ein tägliches Mensa Menü update um 11:00.
                     \n \t /unregister args - Vom täglichen update abmelden. \n
                     \t /printStats - Schicke den aktuellen Status \n \t /menu
                     - Schicke mir das aktuelle Menü. \n \t args: mensa,
                     bistro, qwest \n \t Beispiel: /register mensa bistro""")

from telegram.ext import CommandHandler
start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

title = {'mensa': 'Mensa Menü für heute', 'bistro': 'Bistro Menü für heute',
         'qwest': 'Qwest Menü für heute'}

from html.parser import HTMLParser

class MyHTMLParser(HTMLParser):
    _string = '' 

    def handle_starttag(self, tag, attrs):
        if ('ul' in tag):
            self._string += ':\n'
        if ('li' in tag):
            self._string += '\t+ '
        if ('br' in tag):
            self._string += '\n\t\t'

    def handle_endtag(self, tag):
        if ('li' in tag):
            self._string += '\n'

    def handle_data(self, data):
        if (data and data != '\t' and data != '\n'):
            self._string = self._string + data.replace('\t','').replace('\n','').replace('  ',' ').replace('  ','')
       
    def getString(self):
        return self._string.replace('Aktionen:\n','')


mensa_rss_url = "https://www.akafoe.de/gastronomie/speiseplaene-der-mensen/ruhr-universitaet-bochum/?mid=1&tx_akafoespeiseplan_mensadetails%5Baction%5D=feed&tx_akafoespeiseplan_mensadetails%5Bcontroller%5D=RSSFeed&cHash=4edee1a00c935292eacea989d96e7f6d"
bistro_rss_url = "https://www.akafoe.de/gastronomie/speiseplaene-der-mensen/bistro-der-ruhr-universitaet-bochum/?mid=37&tx_akafoespeiseplan_mensadetails%5Baction%5D=feed&tx_akafoespeiseplan_mensadetails%5Bcontroller%5D=RSSFeed&cHash=00668abfcb211d080183ffcf28623873"
qwest_rss_url = "https://www.akafoe.de/gastronomie/gastronomien/q-west/?mid=38&tx_akafoespeiseplan_mensadetails%5Baction%5D=feed&tx_akafoespeiseplan_mensadetails%5Bcontroller%5D=RSSFeed&cHash=63fc5952700d5cabcb3ad75bb055887d"

def makeMenus():
    feed_mensa = feedparser.parse(mensa_rss_url)
    today_mensa = feed_mensa['items'][date.today().weekday()]['summary']
    title['mensa'] = feed_mensa['items'][date.today().weekday()]['title']
    title['mensa'].replace(", an der Ausgabestelle 'Ruhr-Universität Bochum'",
                           ' Mensa')
    feed_bistro = feedparser.parse(bistro_rss_url)
    today_bistro = feed_bistro['items'][date.today().weekday()]['summary']
    title['bistro'] = feed_bistro['items'][date.today().weekday()]['title']
    title['bistro'].replace(", an der Ausgabestelle 'Ruhr Universität Bochum - Bistro'", " Bistro")
    feed_qwest = feedparser.parse(qwest_rss_url)
    today_qwest = feed_qwest['items'][date.today().weekday()]['summary']
    title['qwest'] = feed_qwest['items'][date.today().weekday()]['title']
    title['qwest'].replace(", an der Ausgabestelle 'Q-West'"," Q-West")
    parser = MyHTMLParser()
    parser.feed(today_mensa)
    mensa = parser.getString()
    parser = MyHTMLParser()
    parser.feed(today_bistro)
    bistro = parser.getString()
    parser = MyHTMLParser()
    parser.feed(today_qwest)
    qwest = parser.getString()
    return {'mensa': mensa, 'bistro': bistro, 'qwest': qwest}
    # return {'mensa': mensa, 'bistro': bistro, 'qwest': qwest.getQWest(date.today().weekday())}
    
    
def menuSendAll():
    today = date.today()
    if (today.weekday() < 5 and today not in de_holidays):
        print('Sending menu to all registered users.')
        text = makeMenus()
        for chat_id in registered_user.keys():
            message = ''
            for arg in registered_user[chat_id]:
                message += title[arg]
                message += '\n' + text[arg] + '\n'
            try:
                updater.bot.send_message(chat_id=chat_id, text=message)
            except Exception as e:
                print(e)
    else:
        print("Not sending Menu because its Weekend or holiday.")

def menu(bot, update):
    chat_id = update.message.chat_id
    message = ''
    for arg in title.keys():
        message += title[arg]
        message += '\n' + text[arg] + '\n'
    bot.send_message(chat_id=chat_id, text=message)

with open('data.pkl', 'rb') as f:
    try:
        registered_user = pickle.load(f)
    except EOFError:
        registered_user = {}
print(registered_user)

def register(bot, update, args):
    chat_id = update.message.chat_id
    if args:
        if chat_id not in registered_user.keys():
            registered_user[chat_id] = args
            bot.send_message(chat_id=chat_id, text="Du bist nun angemeldet für eine Tägliche Nachricht mit dem Menü um 11:00 für " + str(args))
        else:
            for arg in args:
                if arg not in registered_user[chat_id] and arg in title.keys():
                    registered_user[chat_id].append(arg)
                    bot.send_message(chat_id=chat_id, text="Du bist nun angemeldet für eine Tägliche Nachricht mit dem Menü um 11:00 für " + arg)
                else:
                    bot.send_message(chat_id=chat_id, text="Das angegebene Argument ist leider nicht gültig, oder wurde für sie schon registriert.")
        print(registered_user)
        with open('data.pkl', 'wb') as f:
            pickle.dump(registered_user, f)    
    else:
        bot.send_message(chat_id=chat_id, text="Es wurden keine oder falsche Argumente angegeben, mögliche Argumente sind [mensa, bistro]")

def unregister(bot, update, args):
    chat_id = update.message.chat_id
    if args:
        if chat_id in registered_user.keys():
            for arg in args:
                if arg in registered_user[chat_id] and arg in title.keys():
                    del registered_user[chat_id][registered_user[chat_id] == arg]
                    bot.send_message(chat_id=chat_id, text=arg + " wurde erfolgreich aus dem täglichen update gelöscht.")
                else:
                    bot.send_message(chat_id=chat_id, text="Das angegebene Argument ist leider nicht gültig, oder nicht für sie registriert.")
        print(registered_user)
        with open('data.pkl', 'wb') as f:
            pickle.dump(registered_user, f)    
    else:
        bot.send_message(chat_id=chat_id, text="Es wurden keine oder falsche Argumente angegeben, mögliche Argumente sind [mensa, bistro]")

def printArgs(bot, update):
    chat_id = update.message.chat_id
    if chat_id in registered_user.keys():
        bot.send_message(chat_id=chat_id, text="Du bist derzeit angemeldet für: " + str(registered_user[chat_id]))
    else:
        bot.send_message(chat_id=chat_id, text="Du bist derzeit für nichts angemeldet.")


def printStats(bot, update):
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text="Deine Chat ID lautet: " + str(chat_id))
    bot.send_message(chat_id=chat_id, text="Derzeit haben sich " + str(len(registered_user)) + " angemeldet.")

def makeText():
    global text 
    text = makeMenus()
    
    

text = {}
makeText()

register_handler = CommandHandler('register', register, pass_args=True)
dispatcher.add_handler(register_handler)
unregister_handler = CommandHandler('unregister', unregister, pass_args=True)
dispatcher.add_handler(unregister_handler)
menu_handler = CommandHandler('menu', menu)
dispatcher.add_handler(menu_handler)
printArgs_handler = CommandHandler('printArgs', printArgs)
dispatcher.add_handler(printArgs_handler)
printStats_handler = CommandHandler('printStats', printStats)
dispatcher.add_handler(printStats_handler)

updater.start_polling()

schedule.every().day.at('11:00').do(menuSendAll)
schedule.every().day.at('01:00').do(makeText)
de_holidays = holidays.DE(prov='NW')

while True:
    schedule.run_pending()
    time.sleep(58)

